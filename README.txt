-------------
CarRentalApp
-------------
Aplikacja umożliwia m.in.:
	- dodanie nowego samochodu do oferty wypożyczalni
	- usunięcie samochodu z oferty wypożyczalni
	- edytowanie danych samochodu
	- wypożyczenie samochodu przez klienta (tylko jeden klient może wypożyczyć ten sam samochód w danym momencie)
	- oddanie wypożyczonego samochodu

W bazie aplikacji znajdują się 2 tabele: Car oraz CarRentalHistory
Wypożyczenie samochodu oraz zwrot skutkuje dodaniem stosownego wpisu w tabeli z historią. 
Dodałem również kilka przykładowych testów jednostkowych oraz integracyjnych. 

Zastosowałem m.in. bazę danych H2, Hibernate, Swagger. 
Budowałem go z pomocą Gradle. 

Baza danych potrzebna do poprawnego działania aplikacji znajduje się w tym folderze, pod nazwą CarRentalDB.mv 
Ze względu na okrojoną formę aplikacji jest jedna baza, która służy zarówno jako "produkcyjna" jak i do testów integracyjnych.

Po uruchomieniu aplikacji:
Dokumentacja API w SwaggerUI: http://localhost:8080/swagger-ui.html#
Consola H2: http://localhost:8080/h2-console/ (patrz >> h2Console.jpg)
	->  aby przetestować połączenie z bazą lub wejść do konsoli należy w JDBC URL wpisać: jdbc:h2:file:./CarRentalDB 
	-> User Name oraz password pozostawić puste 



