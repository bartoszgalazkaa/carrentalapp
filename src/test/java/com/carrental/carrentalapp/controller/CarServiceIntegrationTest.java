package com.carrental.carrentalapp.controller;

import java.util.NoSuchElementException;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.hamcrest.Matchers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.util.NestedServletException;

import com.carrental.carrentalapp.model.Car;
import com.carrental.carrentalapp.repository.CarRentalHistoryRepository;
import com.carrental.carrentalapp.repository.CarRepository;
import com.carrental.carrentalapp.service.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@SpringBootTest
@AutoConfigureMockMvc
public class CarServiceIntegrationTest {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void GetSpecifiedCar_ExistingId_returnGivenId() throws Exception {
		// given
		// when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/car/1")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
		// then

		Car car = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Car.class);
		assertThat(car.getId()).isEqualTo(1L);
	}

	@Test
	void addCarToOffer_properParams_carAdded() throws Exception {
		// given
		String path = "/car?available=true&brand=Citroen&model=C5&registrationNo=LPU%202090";
		// when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put(path))
						.andDo(MockMvcResultHandlers.print())
						.andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
		// then
		assertThat(mvcResult.getResponse().getContentAsString().equals("Car added"));
				
		}

}
