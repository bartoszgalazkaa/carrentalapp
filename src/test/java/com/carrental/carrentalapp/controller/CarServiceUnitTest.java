package com.carrental.carrentalapp.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import com.carrental.carrentalapp.model.Action;
import com.carrental.carrentalapp.model.Car;
import com.carrental.carrentalapp.model.CarRentalHistory;
import com.carrental.carrentalapp.repository.CarRentalHistoryRepository;
import com.carrental.carrentalapp.repository.CarRepository;
import com.carrental.carrentalapp.service.CarService;
import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.*;
@ExtendWith(MockitoExtension.class)
public class CarServiceUnitTest {
	
	@Mock
	private CarRepository carRepo;
	@Mock
	private CarRentalHistoryRepository rentalHistoryRepo;
	
	@InjectMocks
	private CarService carService;
	
	private static final boolean AVAILABLE = true;
	private static final boolean RENTED = false;
	
	@Test
	void getCars_noArgsMethod_returnCarList(){
		//given 
			when(carRepo.findAll()).thenReturn(Arrays.asList(
					new Car(1L,"brandTest", "modelTest", AVAILABLE, RENTED, "TEST TEST", String.valueOf(LocalDateTime.now())),	
					new Car(2L,"brandTest", "modelTest", AVAILABLE, RENTED, "TEST TEST", String.valueOf(LocalDateTime.now())),
					new Car(3L,"brandTest", "modelTest", AVAILABLE, RENTED, "TEST TEST", String.valueOf(LocalDateTime.now()))	
					));
		//when 
		List<Car> allCars = carService.getCars();
		//then 
		assertThat(allCars).hasSize(3);
		
	}
	
	@Test
	void getScpecifiedCar_notExistingCarid_NoSuchElementException(){
		//given
		//when
		//then 
		assertThrows(NoSuchElementException.class, () -> carService.getSpecifiedCar(1L));
		
		
	}
	
	
	@Test
	void saveActionInHistory_properIdAndAction_returnCarList(){
		//given 
			long id = 1;
			Action action = Action.RENT;
			CarRentalHistory carRentalHistory = new CarRentalHistory(id, String.valueOf(LocalDateTime.now()), action.name());
			when(rentalHistoryRepo.saveAndFlush(Mockito.any(CarRentalHistory.class)))
	              .thenReturn(carRentalHistory);
			
		//when 
			CarRentalHistory result = carService.saveActionInHistory(id, action);
			
		//then 
		assertThat(result.getActionName().equals(action.name()));
		
	}
	
}
