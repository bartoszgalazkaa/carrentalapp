package com.carrental.carrentalapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carrental.carrentalapp.model.Car;
import com.carrental.carrentalapp.model.CarRentalHistory;

@Repository
public interface CarRentalHistoryRepository extends JpaRepository<CarRentalHistory, Long> {

}
