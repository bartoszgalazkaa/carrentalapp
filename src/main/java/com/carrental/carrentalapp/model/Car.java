package com.carrental.carrentalapp.model;

import com.carrental.carrentalapp.model.CarRentalHistory;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity //oznaczam jako encja danych do zarządzania przez Hibernate
@Getter
@Setter
public class Car {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String brand;
	private String model;
	private boolean available;
	private boolean rented;
	private String registrationNo;
	private String timeOfCreation;
	
	public Car() {
		
	}
	
	public Car(String brand, String model, boolean available, boolean rented, String registrationNo,
			String timeOfCreation) {
		super();
		this.brand = brand;
		this.model = model;
		this.available = available;
		this.rented = rented;
		this.registrationNo = registrationNo;
		this.timeOfCreation = timeOfCreation;
		
	}
	
	public Car(long id, String brand, String model, boolean available, boolean rented, String registrationNo,
			String timeOfCreation) {
		super();
		this.id = id;
		this.brand = brand;
		this.model = model;
		this.available = available;
		this.rented = rented;
		this.registrationNo = registrationNo;
		this.timeOfCreation = timeOfCreation;
		
	}
	
	
	@OneToMany
	@JoinColumn(name = "carId")
	private List<CarRentalHistory> carRentalHistory;
	
	
}
