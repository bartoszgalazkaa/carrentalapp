package com.carrental.carrentalapp.model;

public enum Action {
	RENT,
	RETURN,
	SERVICED,
	CLEANED
}
