package com.carrental.carrentalapp.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity // oznaczam jako encja danych do zarządzania przez Hibernate
@Getter
@Setter
public class CarRentalHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long carId;
	private String timeOfCreation;
	private String actionName;

	public CarRentalHistory() {

	}

	public CarRentalHistory(long carId, String timeOfCreation, String actionName) {
		super();
		this.carId = carId;
		this.timeOfCreation = timeOfCreation;
		this.actionName = actionName;
	}

}