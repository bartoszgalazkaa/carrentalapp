package com.carrental.carrentalapp.controller;
import com.carrental.carrentalapp.model.Car;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.carrental.carrentalapp.service.CarService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class CarController {
	
	private final CarService carService;
	
 @GetMapping("/car")
	public List<Car> getCars() {
		return carService.getCars();
	}

 @GetMapping("/car/{id}")
 //(@RequestParam(value = "car_id") String car_id) 
	public Car getSpecifiedCar(@PathVariable long id){
		return carService.getSpecifiedCar(id);
	}
 
 @PostMapping("/car/{id}")
 	public String rentCar(@PathVariable long id) {
 		return carService.rentCar(id);
 	}
 
 @PostMapping("/car-return/{id}")
	public String returnCar(@PathVariable long id) {
		return carService.returnCar(id);
	}
 
 @PostMapping("car-update/{id}")
 public String updateCarData(@PathVariable long id, @RequestParam String fieldToUpdate, @RequestParam String newValue) {
	 return carService.updateCarData(id, fieldToUpdate, newValue);
 }
 
 
 @PutMapping("/car")
 public String addCarToOffer(@RequestParam String brand ,@RequestParam String model
		 							,@RequestParam boolean available ,@RequestParam String registrationNo) {
	 return carService.addCarToOffer(brand, model, available, registrationNo);
 }
 
 @DeleteMapping("/car/{id}")
	public String deleteCarFromOffer(@PathVariable long id) {
		return carService.deleteCarFromOffer(id);
	}
 	
}
 	