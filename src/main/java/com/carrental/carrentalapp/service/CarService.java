package com.carrental.carrentalapp.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;

import org.h2.message.DbException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.carrental.carrentalapp.model.Car;
import com.carrental.carrentalapp.model.CarRentalHistory;
import com.carrental.carrentalapp.repository.CarRentalHistoryRepository;
import com.carrental.carrentalapp.repository.CarRepository;
import com.carrental.carrentalapp.model.Action;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CarService {

	private final CarRepository carRepo;
	private final CarRentalHistoryRepository rentalHistoryRepo;

	
	
	public List<Car> getCars() {
		return carRepo.findAll();
	}

	public Car getSpecifiedCar(long id) {

		return carRepo.findById(id).orElseThrow();
	}


	public String rentCar(long id) {
		String rentedBrand = getSpecifiedCar(id).getBrand();
		String rentedModel = getSpecifiedCar(id).getModel();
		String message = "";
		if (!getSpecifiedCar(id).isRented() && getSpecifiedCar(id).isAvailable()) {
			message = "Your rented car: " + rentedBrand + " " + rentedModel;
			getSpecifiedCar(id).setRented(true);
			
			saveActionInHistory(id, Action.RENT);
			
			carRepo.flush();
		} else {
			message = rentedBrand + " " + rentedModel + " is unavailable now, try another one";
		}

		return message;

	}

	public String returnCar(long id) {
		String rentedBrand = getSpecifiedCar(id).getBrand();
		String rentedModel = getSpecifiedCar(id).getModel();
		String message = "";
		if (getSpecifiedCar(id).isRented()) {
			message = rentedBrand + " " + rentedModel + " is properly returned";
			getSpecifiedCar(id).setRented(false);
			carRepo.flush();
			saveActionInHistory(id, Action.RETURN);
		} else {
			message = rentedBrand + " " + rentedModel + " isn't rented, you can't return it.";
		}

		return message;

	}
	
	
	// String.valueOf(LocalDateTime.now())

	public String addCarToOffer(String brand, String model, boolean available, String registrationNo) {
		String message = "";
		try {
			carRepo.saveAndFlush(new Car(brand, model, available, false, registrationNo, String.valueOf(LocalDateTime.now())));
			message = "Car added";
		} catch (org.springframework.dao.DataIntegrityViolationException exception) {
			message = "Probably you want to add car with existing registration_no in DB";
		}
		return message;
	}

	

	public String deleteCarFromOffer(long id) {

		String message = "";

		try {
			String rentedBrand = getSpecifiedCar(id).getBrand();
			String rentedModel = getSpecifiedCar(id).getModel();
			carRepo.deleteById(id);
			carRepo.flush();
			message = rentedBrand + " " + rentedModel + " is deleted";
		} catch (java.util.NoSuchElementException exception) {
			message = " There's no car with the typed id";
		}
		return message;

	}
	public String updateCarData(long id, String fieldToUpdate, String newValue) {
		String message = " Car with typed id is successfully updated";
		
		if (fieldToUpdate.equals("brand"))
			getSpecifiedCar(id).setBrand(newValue);
		else if (fieldToUpdate.equals("model"))
			getSpecifiedCar(id).setModel(newValue);
		else if (fieldToUpdate.equals("registrationNo"))
			getSpecifiedCar(id).setRegistrationNo(newValue);
		else if (fieldToUpdate.equals("available"))
			getSpecifiedCar(id).setAvailable(Boolean.valueOf(newValue));
		else if (fieldToUpdate.equals("rented"))
			getSpecifiedCar(id).setRented(Boolean.valueOf(newValue));
		else {
			message = " fieldToUpdate is incorrect";
		}
		
		carRepo.flush();
		return message;
		
	}
	
	public List<CarRentalHistory> getCarRentalHistory(){
		return rentalHistoryRepo.findAll();
	}
	
	public CarRentalHistory saveActionInHistory(long carId, Action actionName) {
		return rentalHistoryRepo.saveAndFlush(new CarRentalHistory(carId, String.valueOf(LocalDateTime.now()), actionName.name()));
	}
	
	
	
	
	

}
