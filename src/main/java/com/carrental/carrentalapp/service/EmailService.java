package com.carrental.carrentalapp.service;

import java.util.Properties;
import java.util.logging.FileHandler;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.jboss.logging.Logger;

import ch.qos.logback.classic.Level;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;

public class EmailService {

	public static void sendMail(String recepient) throws MessagingException {
		System.out.println("Preparing to send email");
		Properties properties = new Properties();

		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.host", "poczta.o2.pl");
		properties.put("mail.smtp.port", "587");

		String myEmailAddress = "galazka_bartosz@o2.pl";
		String myEmailAccountPassword = "Kropka1dikanapka";
		
	
		
		
		Session session = Session.getInstance(properties, new Authenticator() {
		
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myEmailAddress, myEmailAccountPassword);
			}
		});

		Message message = prepareMessage(session, myEmailAddress, recepient);
		session.setDebug(true);
		Transport.send(message);
System.out.println("Message sent succesfully");
	}

	private static Message prepareMessage(Session session, String myEmailAddress, String recepient) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myEmailAddress));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject("My first mail from JAVA :) ");
			message.setText("Siemka, w�a�nie wykona�em kolejny krok, \n poszed� mailik z javy :)) ");
			return message;
		} catch (MessagingException ex) {
			Logger.getLogger(EmailService.class.getName()).log(null, Level.ALL, ex);	

		}
		return null;
	}
}
